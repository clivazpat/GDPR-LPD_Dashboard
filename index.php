<?php

namespace tool_rgpdlpd;

use tool_rgpdlpd\Library\Router;

// Define constant
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', realpath(dirname(__FILE__)));
define('APPPATH', realpath(ROOT . DS . 'app'));
define('LIBPATH', realpath(ROOT . DS . 'library'));
define('DEBUG_LEVEL', 2);

define('ABSURL', 'http://localhost/tool_rgpdlpd');

require LIBPATH . DS . 'autoload.php';
require LIBPATH . DS . 'function.php';

//load router
$rooter = new Router();
$rooter->SetErrorReporting();

//Run
$rooter->CallHook();