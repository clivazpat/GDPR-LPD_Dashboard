<?php
namespace tool_rgpdlpd\Library\Database;

use \PDO;

class DbConnect {
    public static function Get() {
    global $pdo;

    $host = "localhost";
    $name = "db_rgpdlpd";
    $user = "root";
    $pass = "";

    if($pdo == null) {
        try {
            $pdo = new PDO('mysql::host='.$host.';dbname='.$name.'', $user, $pass);
            $pdo->exec('SET NAMES utf8');
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $e){
            die('<div align="center">
                    <h1>Error</h1>
                    <h3>Can\'t connect to the database</h3>
                    </div>');
        }
    }
     return $pdo;
    }
}

