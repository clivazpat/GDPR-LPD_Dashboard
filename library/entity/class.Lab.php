<?php
namespace tool_rgpdlpd\Library\Entity;

use tool_rgpdlpd\Library\Database\DbConnect;
use \PDO;

class Lab
{
    var $conn;

    public function __construct()
    {
        $this->conn=DbConnect::Get('Connection');
    }

    public function getAllLabs(){
        $conn = $this->conn;
        $sql="SELECT * FROM lab ORDER BY nom";
        return $conn->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getLabById($id){
        $conn = $this->conn;
        $sql="SELECT id, nom FROM lab WHERE id=:id";
        $stat = $conn->prepare($sql);
        $stat->bindParam(":id", $id);
        $stat->execute();
        return $stat->fetch(PDO::FETCH_LAZY);
    }


}