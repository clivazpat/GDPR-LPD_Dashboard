<?php
namespace tool_rgpdlpd\Library\Entity;

use tool_rgpdlpd\Library\Database\DbConnect;
use \PDO;

class Question
{
    var $conn;

    public function __construct()
    {
        $this->conn=DbConnect::Get('Connection');
    }

    public function getAllQuestions(){
        $conn = $this->conn;
        $sql="SELECT * FROM question";
        return $conn->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    public function updateQuestion($id, $text){
        $conn = $this->conn;
        $sql="UPDATE question SET text=:text WHERE id=:id";
        $stat = $conn->prepare($sql);
        $stat->bindParam(":id",$id);$stat->bindParam(":text",$text);
        $stat->execute();
    }
}