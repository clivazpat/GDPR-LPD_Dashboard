<?php
namespace tool_rgpdlpd\Library\Entity;

use tool_rgpdlpd\Library\Database\DbConnect;
use \PDO;

class Choix
{
    var $conn;

    public function __construct()
    {
        $this->conn=DbConnect::Get('Connection');
    }

    public function getAllChoixByQuestion($id){
        $conn = $this->conn;
        $sql="SELECT * FROM choix WHERE idQuestion=:id";
        $stat = $conn->prepare($sql);
        $stat->bindParam(":id", $id);
        $stat->execute();
        return $stat->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getChoixById($id){
        $conn = $this->conn;
        $sql="SELECT id, text FROM choix WHERE id=:id";
        $stat = $conn->prepare($sql);
        $stat->bindParam(":id", $id);
        $stat->execute();
        return $stat->fetch(PDO::FETCH_LAZY);
    }

    public function updateChoix($id,$text){
        $conn = $this->conn;
        $sql="UPDATE choix SET text=:text WHERE id=:id";
        $stat = $conn->prepare($sql);
        $stat->bindParam(":id",$id);$stat->bindParam(":text",$text);
        $stat->execute();
    }
}