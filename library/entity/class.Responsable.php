<?php
namespace tool_rgpdlpd\Library\Entity;

use tool_rgpdlpd\Library\Database\DbConnect;
use \PDO;

class Responsable
{
    var $conn;

    public function __construct()
    {
        $this->conn=DbConnect::Get('Connection');
    }

    public function getAllResponsables(){
        $conn = $this->conn;
        $sql="SELECT * FROM responsable ORDER BY nom";
        return $conn->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getResponsableById($id){
        $conn = $this->conn;
        $sql="SELECT id, nom FROM responsable WHERE id=:id";
        $stat = $conn->prepare($sql);
        $stat->bindParam(":id", $id);
        $stat->execute();
        return $stat->fetch(PDO::FETCH_LAZY);
    }
}