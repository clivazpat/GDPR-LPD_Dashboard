<?php
namespace tool_rgpdlpd\Library\Entity;

use tool_rgpdlpd\Library\Database\DbConnect;
use \PDO;

class Resultat
{
    var $conn;

    public function __construct()
    {
        $this->conn=DbConnect::Get('Connection');
    }

    public function getResultats($idType, $idChoix, $idPerimetre){
        $conn = $this->conn;
        $sql="SELECT categorie, description FROM resultat WHERE idType=:idType AND idChoix=:idChoix AND idPerimetre=:idPerimetre";
        $stat = $conn->prepare($sql);
        $stat->bindParam(":idType", $idType);$stat->bindParam(":idChoix", $idChoix);$stat->bindParam(":idPerimetre", $idPerimetre);
        $stat->execute();
        return $stat->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllResultats(){
        $conn = $this->conn;
        $sql="SELECT * FROM resultat";
        return $conn->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }


    public function updateResultat($id, $categorie, $description){
        $conn = $this->conn;
        $sql="UPDATE resultat SET categorie=:categorie, description=:description WHERE id=:id";
        $stat = $conn->prepare($sql);
        $stat->bindParam(":id",$id);$stat->bindParam(":categorie",$categorie);$stat->bindParam(":description",$description);
        $stat->execute();
    }

}