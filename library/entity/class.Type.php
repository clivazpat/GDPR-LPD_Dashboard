<?php
namespace tool_rgpdlpd\Library\Entity;

use tool_rgpdlpd\Library\Database\DbConnect;
use \PDO;

class Type
{
    var $conn;

    public function __construct()
    {
        $this->conn=DbConnect::Get('Connection');
    }

    public function getAllTypes(){
        $conn = $this->conn;
        $sql="SELECT * FROM type";
        return $conn->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getTypeById($id){
        $conn = $this->conn;
        $sql="SELECT id, nom FROM type WHERE id=:id";
        $stat = $conn->prepare($sql);
        $stat->bindParam(":id", $id);
        $stat->execute();
        return $stat->fetch(PDO::FETCH_LAZY);
    }
}