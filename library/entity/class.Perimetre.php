<?php
namespace tool_rgpdlpd\Library\Entity;

use tool_rgpdlpd\Library\Database\DbConnect;
use \PDO;

class Perimetre
{
    var $conn;

    public function __construct()
    {
        $this->conn=DbConnect::Get('Connection');
    }

    public function getPerimetreByName($name){
        $conn = $this->conn;
        $sql="SELECT id, description FROM perimetre WHERE nom=:name";
        $stat = $conn->prepare($sql);
        $stat->bindParam(":name", $name);
        $stat->execute();
        return $stat->fetch(PDO::FETCH_LAZY);
    }

    public function getPerimetreById($id){
        $conn = $this->conn;
        $sql="SELECT nom, description FROM perimetre WHERE id=:id";
        $stat = $conn->prepare($sql);
        $stat->bindParam(":id", $id);
        $stat->execute();
        return $stat->fetch(PDO::FETCH_LAZY);
    }
}