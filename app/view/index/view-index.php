<div class="section no-pad-bot" id="index-banner">
    <div class="container">
        <form action="<?php echo ABSURL;?>/index/result" method="post">
            <h4 class="header center light-blue-text">Outil d'aide au RGPD et à la LPD</h4>
            <br>
            <div class="row">
                <div class="input-field col s6">
                    <select name="lab">
                        <option value="" disabled selected>Effectuez une sélection</option>
                        <?php
                        foreach ($labs as $lab) {
                            echo '<option value="'. $lab['id'] . '">' . $lab['nom'] . '</option>';
                        }
                        ?>
                    </select>
                    <label>Lab concerné</label>
                </div>
                <div class="input-field col s6 center-align">
                    <div class="switch">
                        <label>Responsable de traitement</label>
                        </br>
                        <label>
                            Aucun
                            <input type="checkbox" id="switch">
                            <span class="lever"></span>
                            Oui
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <select name="type" required>
                        <option value="" disabled selected>Effectuez une sélection</option>
                        <?php
                        foreach ($types as $type) {
                            echo '<option value="'. $type['id'] . '">' . $type['nom'] . '</option>';
                        }
                        ?>
                    </select>
                    <label>Type de données (obligatoire)</label>
                </div>

                <div class="input-field col s6" id="resp" style="display: none">
                    <select name="responsable">
                        <option value="" disabled selected>Effectuez une sélection</option>
                        <?php
                        foreach ($responsables as $responsable) {
                            echo '<option value="'. $responsable['id'] . '">' . $responsable['nom'] . '</option>';
                        }
                        ?>
                    </select>
                    <label>Personne responsable</label>
                </div>
            </div>

            <br>
            <h5 class="header center light-blue-text">Traitement des données définis légalement</h5>
            <br>
            <table class="striped">

                <tbody>
                    <?php
                    $html = "";                   
                        foreach ($questions as $question) {
                            $html .= '<tr>
                                    <td>' . $question['text'] . '</td>';
                                    foreach ($question['choix'] as $choix) {
                                    $html .=    '<td><label>
                                        <input class="with-gap" name="'. $choix['idQuestion'] .'" type="radio" value="'. $choix['id'] .'" required/>
                                        <span>'. $choix['text'] .'</span>
                                    </label></td>';
                                    }
                            $html .= '</tr>';
                        }
                    echo $html;
                    ?>
                </tbody>
            </table>
            <div class="col s10 offset-s1 center-align">
                <br>
                <br>
                <button class="btn waves-effect waves-light" type="submit" name="submit">Confirmer
                    <i class="material-icons right">description</i>
                </button>
                <br>
                <br>
            </div>
        </form>
    </div>
</div>