<div class="section no-pad-bot" id="index-banner">
    <div class="container">
        <h4 class="header center light-blue-text">Consignes pour le traitement des données</h4>
        <br>
        <div class="col s12 center-align">
            <div class="row">
                <div class="col s12 m6 offset-m3">
                    <div class="card blue-grey darken-3">
                        <div class="card-content white-text left-align">
                            <span class="card-title">Récapitulatif</span>
                            <?php
                                echo "  Lab concerné : $labName<br/>
                                        Type de données : $typeName<br/>
                                        Responsable de traitement : $responsableName
                                    ";
                            ?>
                        </div>
                        <div class="card-action">
                            <!-- Modal Trigger -->
                            <a class="modal-trigger" href="#modal1">Choix effectués</a>
                            <a class="modal-trigger" href="#modal2">Recommencer</a>
                            <!-- Modal Structure -->
                            <div id="modal1" class="modal">
                                <div class="modal-content">
                                    <h4>Choix effectués</h4>
                                    <?php
                                     echo " ".$questions[0]['text']." : <b>".$choix1['text']."</b><br/>".
                                            $questions[1]['text']." : <b> ".$choix2['text']."</b><br/>".
                                            $questions[2]['text']." : <b> ".$choix3['text']."</b><br/>".
                                            $questions[3]['text']." : <b> ".$choix4['text']."</b><br/>".
                                            $questions[4]['text']." : <b> ".$choix5['text']."</b><br/>".
                                            $questions[5]['text']." : <b> ".$choix6['text']."</b><br/>                                      
                                        ";
                                    ?>
                                </div>
                                <div class="modal-footer">
                                    <a href="#!" class="modal-close waves-effect btn-flat">Fermer</a>
                                </div>
                            </div>
                            <div id="modal2" class="modal">
                                <div class="modal-content">
                                    <h4>Êtes-vous sûr ?</h4>
                                    Vos choix seront réinitialsés !
                                </div>
                                <div class="modal-footer">
                                    <a href="<?php echo ABSURL;?>" class="modal-close waves-effect waves-green btn-flat">Confirmer</a>
                                    <a href="#!" class="modal-close waves-effect waves-red btn-flat">Fermer</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <h5 class="header center light-blue-text">Résultats selon chaque périmètre</h5>
            <br>
            <ul class="tabs">
                <li class="tab col s3">
                    <a href="#rgpd">RGPD</a>
                </li>
                <li class="tab col s3">
                    <a class="active" href="#lpd">LPD actuelle</a>
                </li>
                <li class="tab col s3">
                    <a href="#nlpd">Nouvelle LPD</a>
                </li>
            </ul>
        </div>
        <div id="rgpd" class="row">
            <h6 class="header center deep-orange-text">
                <?php echo $perimetreRGPD['description'] ?>
            </h6>
            <br>
            <div class="row">
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resRGPD1 as $resultat1) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">subject</i>'. $resultat1['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat1['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resRGPD2 as $resultat2) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">folder</i>'. $resultat2['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat2['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resRGPD3 as $resultat3) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">location_on</i>'. $resultat3['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat3['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resRGPD4 as $resultat4) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">person</i>'. $resultat4['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat4['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resRGPD5 as $resultat5) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">pan_tool</i>'. $resultat5['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat5['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resRGPD6 as $resultat6) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">build</i>'. $resultat6['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat6['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col s10 offset-s1 center-align">
                <br>
                <br>
                <a class="btn waves-effect waves-light deep-orange" href="https://gdpr.algolia.com/fr/" target="_blank">Plus d'informations
                    <i class="material-icons right">info</i>
                </a>
                <br>
                <br>
            </div>
        </div>
        <div id="lpd" class="row">
            <h6 class="header center deep-orange-text">
                <?php echo $perimetreLPD['description'] ?>
            </h6>
            <br>

            <div class="row">
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resLPD1 as $resultat1) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">subject</i>'. $resultat1['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat1['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resLPD2 as $resultat2) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">folder</i>'. $resultat2['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat2['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resLPD3 as $resultat3) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">location_on</i>'. $resultat3['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat3['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resLPD4 as $resultat4) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">person</i>'. $resultat4['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat4['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resLPD5 as $resultat5) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">pan_tool</i>'. $resultat5['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat5['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resLPD6 as $resultat6) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">build</i>'. $resultat6['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat6['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col s10 offset-s1 center-align">
                <br>
                <br>
                <a class="btn waves-effect waves-light deep-orange" href="https://www.admin.ch/opc/fr/classified-compilation/19920153/index.html" target="_blank">Plus d'informations
                    <i class="material-icons right">info</i>
                </a>
                <br>
                <br>
            </div>
        </div>
        <div id="nlpd" class="row">
            <h6 class="header center deep-orange-text">
                <?php echo $perimetreNLPD['description'] ?>
            </h6>
            <br>
            <div class="row">
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resNLPD1 as $resultat1) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">subject</i>'. $resultat1['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat1['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resNLPD2 as $resultat2) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">folder</i>'. $resultat2['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat2['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resNLPD3 as $resultat3) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">location_on</i>'. $resultat3['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat3['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resNLPD4 as $resultat4) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">person</i>'. $resultat4['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat4['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resNLPD5 as $resultat5) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">pan_tool</i>'. $resultat5['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat5['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
                <div class="col s4">
                    <ul class="collapsible popout" data-collapsible="accordion">
                        <?php
                        foreach ($resNLPD6 as $resultat6) {
                            echo ' <li>
                            <div class="collapsible-header">
                                <i class="material-icons">build</i>'. $resultat6['categorie'] .'</div>
                            <div class="collapsible-body">
                                <span>'. $resultat6['description'] .'</span>
                            </div>
                        </li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col s10 offset-s1 center-align">
                <br>
                <br>
                <a class="btn waves-effect waves-light deep-orange" href="https://www.ejpd.admin.ch/ejpd/fr/home/aktuell/news/2017/2017-09-150.html" target="_blank">Plus d'informations
                    <i class="material-icons right">info</i>
                </a>
                <br>
                <br>
            </div>
        </div>
    </div>
</div>