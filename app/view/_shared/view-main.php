<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Outil RGPD-LPD</title>

    <!--Import de Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import de materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php echo ABSURL;?>/assets/css/materialize.min.css" media="screen,projection"
    />
    <!--CSS-->
    <link type="text/css" rel="stylesheet" href="<?php echo ABSURL;?>/assets/css/style.css" media="screen,projection" />

    <!--Faire savoir au navigateur que le site web est optimisé pour mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!--Ajouter une favicon au site web-->
    <link rel="icon" type="image/x-icon" href="<?php echo ABSURL;?>/assets/images/favicon.ico" />

</head>

<body>
    <header>
        <nav class="light-blue darken-3" role="navigation">
            <div class="nav-wrapper container">
                <a id="logo-container" href="<?php echo ABSURL;?>" class="brand-logo right">
                    <img class="menu-img" src="<?php echo ABSURL;?>/assets/images/logo-hes-so-valais.png">
                </a>
                <ul class="left hide-on-med-and-down">
                    <li><a href="#"><i data-target="slide-out" class="material-icons sidenav-trigger">menu</i></a></li>
                </ul>
            </div>
        </nav>
        <ul id="slide-out" class="sidenav">
            <li><div class="user-view">
                <a href="#"><img class="circle" src="<?php echo ABSURL;?>/assets/images/admin.png"></a>
                <a href="#"><span class="subheader">Administration</span></a>
            </div></li>
            <li><a class="waves-effect" href="<?php echo ABSURL;?>/questions"><i class="material-icons">question_answer</i>Questions</a></li>
            <li><a class="waves-effect" href="<?php echo ABSURL;?>/resultats"><i class="material-icons">done_all</i>Résultats</a></li>
            <li><div class="divider"></div></li>
            <li><a class="waves-effect" href="<?php echo ABSURL;?>"><i class="material-icons">home</i>Accueil</a></li>
        </ul>
    </header>
    <main>
        <!--Affichage du contenu de la page-->
        <?php echo $html?>
    </main>
    <footer class="page-footer blue-grey">
        <div class="footer-copyright">
            <div class="container">
                © 2018 HES-SO Valais-Wallis - created by Patrick Clivaz
            </div>
    </footer>

    <!--JavaScript à la fin du document pour optimiser le chargement-->
    <script type="text/javascript" src="<?php echo ABSURL;?>/assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo ABSURL;?>/assets/js/materialize.min.js"></script>
    <script type="text/javascript" src="<?php echo ABSURL;?>/assets/js/initSelect.js"></script>
    <script type="text/javascript" src="<?php echo ABSURL;?>/assets/js/switch.js"></script>
    <script type="text/javascript" src="<?php echo ABSURL;?>/assets/js/initTabs.js"></script>
    <script type="text/javascript" src="<?php echo ABSURL;?>/assets/js/initCollapsible.js"></script>
    <script type="text/javascript" src="<?php echo ABSURL;?>/assets/js/initModal.js"></script>
    <script type="text/javascript" src="<?php echo ABSURL;?>/assets/js/initSidenav.js"></script>
</body>