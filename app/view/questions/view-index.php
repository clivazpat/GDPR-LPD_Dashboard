<div class="section no-pad-bot" id="index-banner">
    <div class="container">
        <h4 class="header center light-blue-text">Administration des questions</h4>
        <br>
        <table class="striped responsive-table"">
            <thead>
                <tr>
                    <th>Textes</th>
                    <th>Choix 1</th>
                    <th>Choix 2</th>
                    <th>Choix 3</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $html = "";                   
                        foreach ($questions as $question) {
                            $html .= '<tr>
                                    <td>' . $question['text'] . '</td>';
                                    foreach ($question['choix'] as $choix) {
                                    $html .=    '<td>
                                        <span>'. $choix['text'] .'</span>
                                   </td>';
                                    }
                            $html .= '<td>
                            <a href="#modal' . $question['id'] . '" class="waves-effect waves-light btn tooltipped modal-trigger">
                                <i class="large material-icons">edit</i>
                            </a>
                            
                            <div id="modal' . $question['id'] . '" class="modal">
                                <form method="post">
                                    <div class="modal-content">
                                        <h4>Modifier la question</h4>
                                        <br>
                                        <div class="input-field col s6">
                                            <input type="hidden" name="editIdQuestion" value="' . $question['id'] . '">
                                            <input id="question" name="editTextQuestion" type="text" class="validate" value="' . $question['text'] . '" required >
                                            <label for="question">Texte de la question</label>
                                        </div>';
                                        $count = 1;
                                        foreach ($question['choix'] as $choix) {
                                            $html .= '<div class="input-field col s6">
                                            <input type="hidden" name="editIdChoix' . $count .'" value="' . $choix['id'] . '">
                                            <input id="choix" name="editTextChoix' . $count .'" type="text" class="validate" value= "' . $choix['text'] . '" required >
                                            <label for="choix">Choix ' . $count .'</label>
                                            </div>';
                                            $count++;
                                        } 
                                    $html.='</div>
                                    <div class="modal-footer">
                                        <button class="modal-action waves-effect waves-green btn-flat" type="submit" name="submit">'."Mettre à jour".'</button>
                                        <a href="#!" class="modal-close waves-effect waves-red btn-flat">'."Annuler".'</a>
                                    </div>
                                </form>
                            </div></></tr>';
                        }
                    echo $html;
                    ?>
                </tr>
            </tbody>
        </table>
        <br>
    </div>
</div>