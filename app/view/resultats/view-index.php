<div class="section no-pad-bot" id="index-banner">
    <div class="container">
        <h4 class="header center light-blue-text">Administration des résultats</h4>
        <br>
        <table class="striped responsive-table">
            <thead>
                <tr>
                    <th>Type de données</th>
                    <th>Choix correspondant</th>
                    <th>Périmètre légal</th>
                    <th>Catégorie</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $html = "";                   
                        foreach ($resultats as $resultat) {
                            $html .= '<tr>
                                    <td>' . $resultat['nameType'] . '</td>
                                    <td>'. $resultat['nameChoix'] .'</td>
                                    <td>'. $resultat['namePerimetre'] .'</td>
                                    <td>'. $resultat['categorie'] .'</td>
                                    <td>'. $resultat['description'] .'</td>
                                    <td>
                                    <a href="#modal' . $resultat['id'] . '" class="waves-effect waves-light btn tooltipped modal-trigger">
                                        <i class="large material-icons">edit</i>
                                    </a>';
                            
                                    $html .= '<div id="modal' . $resultat['id'] . '" class="modal">
                                        <form method="post">
                                            <div class="modal-content">
                                                <h4>Modifier le résultat</h4>
                                                <br>
                                                <div class="input-field col s6">
                                                    <input type="hidden" name="editId" value="' . $resultat['id'] . '">
                                                    <input id="type" name="idType" type="text" class="validate" value="' . $resultat['nameType'] . '" disabled >
                                                    <label for="type">Type de données</label>
                                                </div>
                                                <div class="input-field col s6">
                                                    <input id="choix" name="idChoix" type="text" class="validate" value="' . $resultat['nameChoix'] . '" disabled >
                                                    <label for="choix">Choix correspondant</label>
                                                </div>
                                                <div class="input-field col s6">
                                                    <input id="perimetre" name="idPerimetre" type="text" class="validate" value="' . $resultat['namePerimetre'] . '" disabled >
                                                    <label for="perimetre">Périmetre légal</label>
                                                </div>
                                                <div class="input-field col s6">
                                                    <input id="categorie" name="editCategorie" type="text" class="validate" value="' . $resultat['categorie'] . '" required >
                                                    <label for="categorie">Catégorie</label>
                                                </div>
                                                <div class="input-field col s6">
                                                    <input id="description" name="editDescription" type="text" class="validate" value="' . $resultat['description'] . '" required >
                                                    <label for="description">Description</label>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="modal-action waves-effect waves-green btn-flat" type="submit" name="submit">'."Mettre à jour".'</button>
                                                <a href="#!" class="modal-close waves-effect waves-red btn-flat">'."Annuler".'</a>
                                            </div>
                                        </form>
                                    </div></></tr>';
                        }
                    echo $html;
                    ?>
                </tr>
            </tbody>
        </table>
        <br>
    </div>
</div>