<?php
namespace tool_rgpdlpd\App\Controller;

use tool_rgpdlpd\Library\Mvc\Controller;

class ControllerQuestions extends Controller {

    public function index(){
         
        if (isset($_POST['submit'])) {
            $this->model->updateQuestion($_POST['editIdQuestion'], $_POST['editTextQuestion']);

            $this->model->updateChoix($_POST['editIdChoix1'], $_POST['editTextChoix1']);
            $this->model->updateChoix($_POST['editIdChoix2'], $_POST['editTextChoix2']);
            $this->model->updateChoix($_POST['editIdChoix3'], $_POST['editTextChoix3']);
        }

        $questions = $this->model->getAllQuestions();

        for ($i = 0; $i < count($questions); $i++) {
            $choix = $this->model->getAllChoixByQuestion($questions[$i]['id']);
            $questions[$i]['choix'] = $choix;
        } 

        $this->view->Set('questions', $questions);   

        return $this->view->Render();
    }

}