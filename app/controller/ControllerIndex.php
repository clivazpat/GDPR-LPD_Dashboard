<?php
namespace tool_rgpdlpd\App\Controller;

use tool_rgpdlpd\Library\Mvc\Controller;

class ControllerIndex extends Controller {

    public function index(){

        $labs = $this->model->getAllLabs();
        $this->view->Set('labs', $labs);

        $types = $this->model->getAllTypes();
        $this->view->Set('types', $types);

        $responsables = $this->model->getAllResponsables();
        $this->view->Set('responsables', $responsables);

        $questions = $this->model->getAllQuestions();

        for ($i = 0; $i < count($questions); $i++) {
            $choix = $this->model->getAllChoixByQuestion($questions[$i]['id']);
            $questions[$i]['choix'] = $choix;
        } 

        $this->view->Set('questions', $questions);           

        return $this->view->Render();
    }

    public function result(){

    if(isset($_POST['submit'])){
        if(!empty($_POST['lab'])) {
            $labName = $this->model->getLabById($_POST['lab'])['nom'];
            $this->view->Set('labName', $labName);
        } else { 
            $labName = "Aucun";
            $this->view->Set('labName', $labName);
        }

        if(!empty($_POST['type'])) {
            $typeName = $this->model->getTypeById($_POST['type'])['nom'];
            $this->view->Set('typeName', $typeName);
        } else { 
        $labName = "Aucun";
        $this->view->Set('typeName', $typeName);
        }

        if(!empty($_POST['responsable'])) {
            $responsableName = $this->model->getResponsableById($_POST['responsable'])['nom'];
            $this->view->Set('responsableName', $responsableName);
        } else { 
        $responsableName = "Aucun";
        $this->view->Set('responsableName', $responsableName);
        }

        if(isset($_POST['1']))
        {
            $choix1 = $this->model->getChoixById($_POST['1']);
            $this->view->Set('choix1', $choix1);
        }
        else { 
            $choix1 = "Aucune case cochée";
            $this->view->Set('choix1', $choix1);
        }

        if(isset($_POST['2']))
        {
            $choix2 = $this->model->getChoixById($_POST['2']);
            $this->view->Set('choix2', $choix2);
        }
        else { 
            $choix2 = "Aucune case cochée";
            $this->view->Set('choix2', $choix2);
        }

        if(isset($_POST['3']))
        {
            $choix3 = $this->model->getChoixById($_POST['3']);
            $this->view->Set('choix3', $choix3);
        }
        else { 
            $choix3 = "Aucune case cochée";
            $this->view->Set('choix3', $choix3);
        }

        if(isset($_POST['4']))
        {
            $choix4 = $this->model->getChoixById($_POST['4']);
            $this->view->Set('choix4', $choix4);
        }
        else { 
            $choix4 = "Aucune case cochée";
            $this->view->Set('choix4', $choix4);
        }

        if(isset($_POST['5']))
        {
            $choix5 = $this->model->getChoixById($_POST['5']);
            $this->view->Set('choix5', $choix5);
        }
        else { 
            $choix5 = "Aucune case cochée";
            $this->view->Set('choix5', $choix5);
        }

        if(isset($_POST['6']))
        {
            $choix6 = $this->model->getChoixById($_POST['6']);
            $this->view->Set('choix6', $choix6);
        }
        else { 
            $choix6 = "Aucune case cochée";
            $this->view->Set('choix6', $choix6);
        }
    }


        $perimetreRGPD = $this->model->getPerimetreByName('rgpd');
        $this->view->Set('perimetreRGPD', $perimetreRGPD);

        $perimetreLPD = $this->model->getPerimetreByName('lpd');
        $this->view->Set('perimetreLPD', $perimetreLPD);

        $perimetreNLPD = $this->model->getPerimetreByName('nlpd');
        $this->view->Set('perimetreNLPD', $perimetreNLPD);

        $questions = $this->model->getAllQuestions();
        $this->view->Set('questions', $questions);  


        $resRGPD1 = $this->model->getResultats($_POST['type'], $choix1['id'], $perimetreRGPD['id']);
        $this->view->Set('resRGPD1', $resRGPD1);

        $resRGPD2 = $this->model->getResultats($_POST['type'], $choix2['id'], $perimetreRGPD['id']);
        $this->view->Set('resRGPD2', $resRGPD2);

        $resRGPD3 = $this->model->getResultats($_POST['type'], $choix3['id'], $perimetreRGPD['id']);
        $this->view->Set('resRGPD3', $resRGPD3);

        $resRGPD4 = $this->model->getResultats($_POST['type'], $choix4['id'], $perimetreRGPD['id']);
        $this->view->Set('resRGPD4', $resRGPD4);

        $resRGPD5 = $this->model->getResultats($_POST['type'], $choix5['id'], $perimetreRGPD['id']);
        $this->view->Set('resRGPD5', $resRGPD5);

        $resRGPD6 = $this->model->getResultats($_POST['type'], $choix6['id'], $perimetreRGPD['id']);
        $this->view->Set('resRGPD6', $resRGPD6);


        $resLPD1 = $this->model->getResultats($_POST['type'], $choix1['id'], $perimetreLPD['id']);
        $this->view->Set('resLPD1', $resLPD1);

        $resLPD2 = $this->model->getResultats($_POST['type'], $choix2['id'], $perimetreLPD['id']);
        $this->view->Set('resLPD2', $resLPD2);

        $resLPD3 = $this->model->getResultats($_POST['type'], $choix3['id'], $perimetreLPD['id']);
        $this->view->Set('resLPD3', $resLPD3);

        $resLPD4 = $this->model->getResultats($_POST['type'], $choix4['id'], $perimetreLPD['id']);
        $this->view->Set('resLPD4', $resLPD4);

        $resLPD5 = $this->model->getResultats($_POST['type'], $choix5['id'], $perimetreLPD['id']);
        $this->view->Set('resLPD5', $resLPD5);

        $resLPD6 = $this->model->getResultats($_POST['type'], $choix6['id'], $perimetreLPD['id']);
        $this->view->Set('resLPD6', $resLPD6);


        $resNLPD1 = $this->model->getResultats($_POST['type'], $choix1['id'], $perimetreNLPD['id']);
        $this->view->Set('resNLPD1', $resNLPD1);

        $resNLPD2 = $this->model->getResultats($_POST['type'], $choix2['id'], $perimetreNLPD['id']);
        $this->view->Set('resNLPD2', $resNLPD2);

        $resNLPD3 = $this->model->getResultats($_POST['type'], $choix3['id'], $perimetreNLPD['id']);
        $this->view->Set('resNLPD3', $resNLPD3);

        $resNLPD4 = $this->model->getResultats($_POST['type'], $choix4['id'], $perimetreNLPD['id']);
        $this->view->Set('resNLPD4', $resNLPD4);

        $resNLPD5 = $this->model->getResultats($_POST['type'], $choix5['id'], $perimetreNLPD['id']);
        $this->view->Set('resNLPD5', $resNLPD5);

        $resNLPD6 = $this->model->getResultats($_POST['type'], $choix6['id'], $perimetreNLPD['id']);
        $this->view->Set('resNLPD6', $resNLPD6);


        return $this->view->Render();
    }

}