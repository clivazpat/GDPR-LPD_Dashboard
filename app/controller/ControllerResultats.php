<?php
namespace tool_rgpdlpd\App\Controller;

use tool_rgpdlpd\Library\Mvc\Controller;

class ControllerResultats extends Controller {

    public function index(){

        if (isset($_POST['submit'])) {
            $this->model->updateResultat($_POST['editId'], $_POST['editCategorie'], $_POST['editDescription']);
        }

        $resultats = $this->model->getAllResultats();
        
        $this->view->Set('resultats', $resultats);    

        return $this->view->Render();
    }

}