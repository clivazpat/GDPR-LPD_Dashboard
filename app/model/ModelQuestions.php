<?php
namespace tool_rgpdlpd\App\Model;

use tool_rgpdlpd\Library\Mvc\Model;
use tool_rgpdlpd\Library\Entity\Question;
use tool_rgpdlpd\Library\Entity\Choix;

class ModelQuestions extends Model {

    /**
     * Méthode permettant de récupérer la liste de tous les questions
     * @return array       Liste des questions
     */
    public function getAllQuestions()
    {
        $questionManager = new Question();
        $questions = $questionManager->getAllQuestions();

        return $questions;
    }

    /**
     * Méthode permettant de récupérer la liste de tous les choix selon leur question
     * @param $id          int représentant l'id de la question
     * @return array       Liste des choix
     */
    public function getAllChoixByQuestion($id)
    {
        $choixManager = new Choix();
        $choix = $choixManager->getAllChoixByQuestion($id);

        return $choix;
    }

    /**
     * Méthode permettant de mettre à jour une question
     * @param $id       int représentant l'id de la question
     * @param $text     string représentant le text de la question
     */
    public function updateQuestion($id, $text) {
        $questionManager = new Question();
        return $questionManager->updateQuestion($id, $text);
    }

    /**
     * Méthode permettant de mettre à jour un choix
     * @param $id       int représentant l'id du choix
     * @param $text     string représentant le text du choix
     */
    public function updateChoix($id, $text) {
        $choixManager = new Choix();
        return $choixManager->updateChoix($id, $text);
    }


}