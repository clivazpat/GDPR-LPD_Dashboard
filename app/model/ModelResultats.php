<?php
namespace tool_rgpdlpd\App\Model;

use tool_rgpdlpd\Library\Mvc\Model;
use tool_rgpdlpd\Library\Entity\Type;
use tool_rgpdlpd\Library\Entity\Choix;
use tool_rgpdlpd\Library\Entity\Perimetre;
use tool_rgpdlpd\Library\Entity\Resultat;

class ModelResultats extends Model {

    /**
     * Méthode permettant de récupérer la liste de tous les résultats
     * @return array       Liste des résultats
     */
    public function getAllResultats()
    {
        $resultatManager = new Resultat();
        $resultats = $resultatManager->getAllResultats(); 

        foreach ($resultats as &$resultat) {
            $nameType = $this->getTypeById($resultat['idType']);
            $resultat['nameType'] = $nameType;

            $nameChoix = $this->getChoixById($resultat['idChoix']);
            $resultat['nameChoix'] = $nameChoix;

            $namePerimetre = $this->getPerimetreById($resultat['idPerimetre']);
            $resultat['namePerimetre'] = $namePerimetre;
         }
         

        return $resultats;
    }

    /**
     * Méthode permettant de mettre à jour un résultat
     * @param $id           int rerésentant l'id d'un résultat
     * @param $categorie    string représentant la catégorie d'un résultat
     * @param $description  string représentant la description d'un résultat
     */
    public function updateResultat($id, $categorie, $description) {
        $resultatManager = new Resultat();
        return $resultatManager->updateResultat($id, $categorie, $description);
    }

    /**
     * Méthode permettant de récupérer le nom d'un type selon son id
     * @return Object       Type de données
     */
    public function getTypeById($id)
    {
        $typeManager = new Type();
        $type = $typeManager->getTypeById($id);

        return $type['nom'];
    }

    /**
     * Méthode permettant de récupérer un choix selon son id
     * @return Object       Choix
     */
    public function getChoixById($id)
    {
        $choixManager = new Choix();
        $choix = $choixManager->getChoixById($id);

        return $choix['text'];
    }

        /**
     * Méthode permettant de récupérer un périmètre selon son id
     * @return Object       Perimetre
     */
    public function getPerimetreById($id)
    {
        $perimetreManager = new Perimetre();
        $perimetre = $perimetreManager->getPerimetreById($id);

        return $perimetre['nom'];
    }
}