<?php
namespace tool_rgpdlpd\App\Model;

use tool_rgpdlpd\Library\Mvc\Model;
use tool_rgpdlpd\Library\Entity\Lab;
use tool_rgpdlpd\Library\Entity\Type;
use tool_rgpdlpd\Library\Entity\Responsable;
use tool_rgpdlpd\Library\Entity\Question;
use tool_rgpdlpd\Library\Entity\Choix;
use tool_rgpdlpd\Library\Entity\Perimetre;
use tool_rgpdlpd\Library\Entity\Resultat;

class ModelIndex extends Model {

    /**
     * Méthode permettant de récupérer la liste de tous les labs
     * @return array       Liste des labs
     */
    public function getAllLabs()
    {
        $labManager = new Lab();
        $labs = $labManager->getAllLabs();

        return $labs;
    }

    /**
     * Méthode permettant de récupérer un lab selon son id
     * @return Object       Lab
     */
    public function getLabById($id)
    {
        $labManager = new Lab();
        $lab = $labManager->getLabById($id);

        return $lab;
    }

    /**
     * Méthode permettant de récupérer la liste de tous les types
     * @return array       Liste des types
     */
    public function getAllTypes()
    {
        $typeManager = new Type();
        $types = $typeManager->getAllTypes();

        return $types;
    }

    /**
     * Méthode permettant de récupérer le nom d'un type selon son id
     * @return Object       Type de données
     */
    public function getTypeById($id)
    {
        $typeManager = new Type();
        $type = $typeManager->getTypeById($id);

        return $type;
    }

    /**
     * Méthode permettant de récupérer la liste de tous les responsables
     * @return array       Liste des responsables
     */
    public function getAllResponsables()
    {
        $responsableManager = new Responsable();
        $responsables = $responsableManager->getAllResponsables();

        return $responsables;
    }

    /**
     * Méthode permettant de récupérer un responsable selon son id
     * @return Object       Responsable du traitement
     */
    public function getResponsableById($id)
    {
        $responsableManager = new Responsable();
        $responsable = $responsableManager->getResponsableById($id);

        return $responsable;
    }

    /**
     * Méthode permettant de récupérer la liste de tous les questions
     * @return array       Liste des questions
     */
    public function getAllQuestions()
    {
        $questionManager = new Question();
        $questions = $questionManager->getAllQuestions();

        return $questions;
    }

    /**
     * Méthode permettant de récupérer la liste de tous les choix selon leur question
     * @param $id          int représentant l'id de la question
     * @return array       Liste des choix
     */
    public function getAllChoixByQuestion($id)
    {
        $choixManager = new Choix();
        $choix = $choixManager->getAllChoixByQuestion($id);

        return $choix;
    }

    /**
     * Méthode permettant de récupérer un choix selon son id
     * @return Object       Choix
     */
    public function getChoixById($id)
    {
        $choixManager = new Choix();
        $choix = $choixManager->getChoixById($id);

        return $choix;
    }

    /**
     * Méthode permettant de récupérer la un périmètre selon son nom
     * @return Object       Périmètre
     */
    public function getPerimetreByName($name)
    {
        $perimetreManager = new Perimetre();
        $perimetre = $perimetreManager->getPerimetreByName($name);

        return $perimetre;
    }

    /**
     * Méthode permettant de récupérer les résultats selon l'id du type, choix et perimetre
     * @return array       Liste des résultats
     */
    public function getResultats($idType, $idChoix, $idPerimetre)
    {
        $resultatsManager = new Resultat();
        $resultats = $resultatsManager->getResultats($idType, $idChoix, $idPerimetre);

        return $resultats;
    }
    
}